
using LinearAlgebra
using Otter

# Warning: this code will take minutes to (maybe) hours to complete

# Sinkhorn distance of 256*256 pixel images
n = 256

# Create two "images", i.e. probability vectors with n^2 components
x = range(0, 2pi, length = n*n)
a = normalize(sin.(10x).^2 .+ 0.1rand(n^2) .+ 1e-5, 1)
b = normalize(cos.(2x).^2 .+ 0.1rand(n^2) .+ 1e-5, 1)

# Create a uniform grid cost object. Initialized this way, the cost corresponds
# to the squared error loss on a uniform grid with maximal value 1
c = UniformGrid(n, n)

# Set some of the parameters for the optimization

# First: Options that also apply to sinkhorn_scale
reg = 1e-6     # target value for the regularization
tol = 1e-3     # tolerance that serves as stopping criterion in the final iteration
stol = 1e-2    # tolerance for all but the final iteration
scale = 0.5    # the value of reg is reduced by this factor for each ε-scaling step
trim = 1e-15   # values below trim are considered to be zero

# Second: Options specifically for sinkhorn_multires
partitions = [16, 8, 4, 2, 1] # 8×8, 4×4, 2×2, and 1×1 blocks are used as partitions
sharpness = 25            # proceed to better resolutions whenever one point of
                          # mass is split to no more than 25 pixels on average

# In the background, many sinkhorn_stable steps will be carried out,
# with either different values of ε or with different partitions.
ret = sinkhorn_multires(a, b, c,
                        reg = reg, startreg = 0.01, maxsteps = 200000,
                        tol = tol, scaletol = stol,
                        partitions = partitions, sharpness = sharpness, 
                        trim = trim, scale = scale)

# Get some information about the run
@show cost(ret) 
@show sparsity(ret)
@show steps(ret)
@show runtime(ret)

