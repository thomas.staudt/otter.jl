
# Otter.jl - Optimal TransporT with Entropic Regularization in julia

This package implements performant and memory-efficient versions of the Sinkhorn
algorithm as proposed by [Bernd Schmitzer](https://arxiv.org/abs/1610.06519).

## Usage

Since the package is not officially registered, it must be installed via
```julia
import Pkg; Pkg.add("https://gitlab.gwdg.de/thomas.staudt/otter.jl")
```
Afterwards, you can type `using Otter` to use the package.
The most important functions that Otter provides are `sinkhorn_naive`,
`sinkhorn_stable`, `sinkhorn_scale`, and `sinkhorn_multires`. These functions
implement variations of the Sinkhorn scaling algorithm with increasing
sophistication, based on [this article](https://arxiv.org/abs/1610.06519) by
Bernd Schmitzer. Note, however, that certain details (like stopping criteria or
how to decide on resolution changes) may deviate from Schmitzer's proposals.

All of the `sinkhorn_*` functions take three positional arguments: two
probability vectors `a` and `b` and a cost object `c`. The latter can be
a general cost matrix or a specialized cost type like `UniformGrid`, which is
(much) more memory efficient in some situations. Each call to a `sinkhorn_*`
function also requires the keyword argument `reg`, which is the targeted value
of the regularization parameter (that is often called ε). Other optional keyword
arguments adapt the behavior of the algorithms and are documented in the help
strings for the respective functions.

A simple example, where the naive Sinkhorn algorithm is applied to random
probability vectors with random costs, looks as follows:

```julia
# create two random probability vectors a and b
a, b = rand(10), rand(10) 
a, b = a / sum(a), b / sum(b)

# create a cost matrix
c = [ (i-j)^2 for i in 1:10, j in 1:10 ]

# application of the algorithm in its naive flavor
r = sinkhorn_naive(a, b, c, reg = 0.1)
```

All `sinkhorn_*` functions return a value of type `Otter.Return`, which contains the
results as well as some meta-information collected during the algorithm. Some of
the quantities that can be obtained from the object `r` in the example above
are:

```julia
plan(r)         # the transport plan
cost(r)         # the sinkhorn cost sum(plan(r) .* c)
gradients(r)    # tuple of gradiants (da, db), which are also the dual solutions
converged(r)    # boolean value indicating whether the algorithm converged or not
steps(r)        # the total number of Sinkhorn scaling steps
runtime(r)      # the runtime taken by the Sinkhorn scaling steps
sparsity(r)     # the sparsity of the final transport plan
status(r)       # a status tuple that contains all metadata
```

**Note:** The performance of `sinkhorn_scale` and `sinkhorn_multiscale`, when
applied to large problems with small regularization values, crucially relies on
the performance of sparse-matrix-dense-vector multiplication. For this reason,
you may gain performance by using the
[MKLSparse](https://github.com/JuliaSparse/MKLSparse.jl) package. Another way to
speed up the computations is to use GPU-based arrays. Currently, the
[CUDA](https://github.com/JuliaGPU/CUDA.jl) package is supported for this
purpose (see below).

## GPU support
When the CUDA package is loaded before `using Otter` is issued, GPU support is
enabled. To make use of the GPU, the keyword argument `atype = "gpu"` (for
`Float32`) or `atype = "gpu64"` (for `Float64`) has to be specified when calling
the `sinkhorn_*` routines.  Both dense and sparse kernels are supported. The
boost in performance can be significant (~ factor 10), especially for larger
problems on fast GPUs.

Note that most GPUs are much faster when operating on `Float32` compared to
`Float64`. At the same time, using `Float32` makes the algorithm more vulnerable
to numerical instabilities. For this reason, one should always use
`sinkhorn_stable` as (backend-)algorithm, and set an appropriate absorption
threshold, like `threshold = 1e20`.

## Usage from python

Otter can easily be used from python. First, make sure that julia and Otter are
properly installed on your system. Then, install the
[pyjulia](https://github.com/JuliaPy/pyjulia) python package, e.g., via `pip
install julia`. On your first usage, open a python shell and type
```python
>>> import julia
>>> julia.install()
```

After this, you are good to go and can use all functionality of Otter in python
by importing it. A more sophisticated example:

```python
>>> import numpy as np
>>> from julia import Otter
>>> # create probability vectors
>>> a = np.random.rand(1000)
>>> b = np.random.rand(1000)
>>> a = a / a.sum()
>>> b = b / b.sum()
>>> # create a cost matrix
>>> c = np.random.rand(1000, 1000)
>>> # use Otter to solve the Sinkhorn problem
>>> r = Otter.sinkhorn_scale(a, b, c, startreg = 0.1, reg = 1e-4, scaletol = 1e-2, tol = 1e-5, sparsify = 0.8, trim = 1e-12)
>>> Otter.steps(r) # how many Sinkhorn steps in total
>>> v = Otter.cost(r)  # final cost value
>>> p = Otter.plan(r)  # transport plan as numpy array
```

If CUDA is installed, then `from julia import CUDA` can be used to enable GPU
support in python, too.

Users who are not familiar with julia might be confused by the slow runtime of
the code above. The reason is that julia just-in-time-compiles (jit) the code as
it is executed. The second time `sinkhorn_scale` is executed (in the same
session!), it will be much faster. The compile times are particularly high if
CUDA is used.

**Note:** Currently, sparse julia arrays are converted to *dense* numpy arrays
when `Otter.plan` is issued. This might result in bad performance or high memory
usage.

## Usage from R

Otter can also be used from the
[R programming language](https://www.r-project.org/about.html). Again, make sure
that a current version of julia and Otter are installed correctly. Then, install
the [JuliaCall](https://cran.r-project.org/web/packages/JuliaCall) package in
R via `install.packages("JuliaCall")`. After this, you can use Otter as follows:
```R
> library(JuliaCall)
> julia_setup()
> julia_library("Otter")
> a = runif(1000); a = a / sum(a)
> b = runif(1000); b = b / sum(b)
> c = matrix(runif(1000*1000), ncol = 1000, nrow = 1000)
> r = julia_call("sinkhorn_stable", a, b, c, reg = 1e-1, tol = 1e-4, maxsteps = 100)
> v = julia_call("cost", r)
> p = julia_call("plan", r)
```
Note that the same remarks as for the python code above remain valid.

## Documentation

You can access the documentation of the Sinkhorn routines in the julia command
line. For example, 
```julia
julia> using Otter
julia> ?sinkhorn_stable
```
shows the documentation of `sinkhorn_stable`, including all keyword arguments.

