
# The following are low-level implementations of different flavours of sinkhorn
# iterations. They implement the logic but do not take care of type conversions
# and safety checks, and should therefore not be called by the user directly.
# The routines are to return the final kernel object, a synced dual buffer, and
# a status tuple


# ----- Sinkhorn Naive ------------------------------------------------------ #

"""
Naive implementation of the Sinkhorn algorithm for optimal transport. The
vectors `a` and `b` are assumed to be probability vectors. Only the entry `d.v`
of the dual buffer `d` is used as input for the algorithm. The `control` tuple
must contain the entries

- `:reg`: regularization parameter
- `:error`: function mapping `(u, u_old)` and `(v, v_old)` to an error value
- `:tolerance`: tolerance for the error value before declaring convergence
- `:interval`: number of steps between calls to the error function
- `:maxsteps`: maximal number of Sinkhorn iterations

The function returns a tuple `(k, d, s)` where
- `k`: final kernel
- `d`: final dual buffer
- `s`: status tuple with entries `reg`, `steps`, `error`, `time`, `converged`,
  and `kernelinfo`

Note that this function assumes compatible types for `a, b, k, d, reg` and
should not be called by the user directly.
"""

function _sinkhorn_naive( a :: AbstractVector
                        , b :: AbstractVector
                        , k :: AbstractKernel # uninitialized kernel
                        , d :: DualBuffer     # partially synced dual buffer
                        , control )

  # Prepare / initialize the kernel
  prepare!(k, control.reg)

  # Get backup arrays for u and v
  u_old, v_old = similar(d.u), similar(d.v)

  # Symmetric Sinkhorn updates
  if control.symmetric
    u_buf, v_buf = similar(d.u), similar(d.v)
  end

  # Prepare the main loop
  n = 0
  err = NaN
  converged = false

  # Main loop with time measurement
  dt = @elapsed while n < control.maxsteps && !converged

    n += 1

    if n % control.interval == 0 || n == control.maxsteps

      u_old .= d.u
      v_old .= d.v

      if control.symmetric
        proxystep_symmetric!(d.u, d.v, u_buf, v_buf, a, b, k)
      else
        proxystep!(d.u, d.v, a, b, k)
      end

      err = control.error(d.u, u_old, d.v, v_old)

      if !isfinite(err)

        error("error value $err encountered in iteration $n")

      elseif err <= control.tolerance 

        converged = true

      end

    else

      proxystep!(d.u, d.v, a, b, k)

    end

  end

  # Sync the dual buffer
  sync_dual_buffer!(d, control.reg, u = d.u, v = d.v)

  # Absorb the dual variables in the kernel
  absorb!(k, d.da, d.db, control.reg)

  # Trim the kernel if the algorithm converged
  if converged
    trim!(k, control.trim)

  # Print a warning if the algorithm did not converge
  else
    @warn "algorithm `:naive` with regularization " *
          "$(control.reg) failed to converge in $n steps"
  end

  # Get status information about the optimization process
  status = ( reg = control.reg
           , steps = n
           , error = err
           , time = dt
           , symmetric = control.symmetric
           , algorithm = :naive
           , converged = converged
           , kernelinfo = kernelinfo(k, control.trim) )

  # Return the kernel, the dual buffer, and the status
  k, d, status

end


# ----- Sinkhorn Stable ------------------------------------------------------ #
"""
Stabilized implementation of the Sinkhorn algorithm for optimal transport. The
vectors `a` and `b` are assumed to be probability vectors. The dual variables
`d.da` and `d.db` of the dual buffer `d` are used as input for the algorithm.
The `control` tuple must contain the entries

- `:reg`: regularization parameter
- `:error`: function mapping `(u, u_old)` and `(v, v_old)` to an error value
- `:tolerance`: tolerance for the error value before declaring convergence
- `:interval`: number of steps between calls to the error function
- `:maxsteps`: maximal number of Sinkhorn iterations
- `:threshold`: if `u` or `v` exceed this value, an absorption is triggered

The function returns analog values to `_sinkhorn_naive`, only that the
status also contains the entry `absorptions`.

Note that this function assumes compatible types for `a, b, k, d, reg` and
should not be called by the user directly.
"""

function _sinkhorn_stable( a :: AbstractVector
                         , b :: AbstractVector
                         , k :: AbstractKernel # uninitialized kernel
                         , d :: DualBuffer     # partially synced dual buffer
                         , control )

  # Prepare the kernel, absorbing the auxiliary duals da, db
  prepare!(k, d.da, d.db, control.reg)

  # Prepare the stabilized scaling variables
  fill!.((d.u, d.v), 1)

  # Copies of u and v used for convergence-checking
  u_old, v_old = similar.((d.u, d.v))

  # Symmetric sinkhorn updates require additional buffer
  if control.symmetric
    u_buf, v_buf = similar(d.u), similar(d.v)
  end

  # Prepare the main loop
  n = 0
  absorptions = 0
  converged = false
  err = NaN

  # Main loop
  dt = @elapsed while !converged && n < control.maxsteps

    n += 1

    # Convergence check and update of u and v
    if n % control.interval == 0 || n == control.maxsteps

      u_old .= d.u
      v_old .= d.v

      if control.symmetric
        proxystep_symmetric!(d.u, d.v, u_buf, v_buf, a, b, k)
      else
        proxystep!(d.u, d.v, a, b, k)
      end

      err = control.error(d.u, u_old, d.v, v_old)

      if !isfinite(err)

        Base.error("Error value $err encountered in iteration $n")

      elseif err <= control.tolerance 

        converged = true
      end

    else

      if control.symmetric
        proxystep_symmetric!(d.u, d.v, u_buf, v_buf, a, b, k)
      else
        proxystep!(d.u, d.v, a, b, k)
      end

    end

    # Absorption steps
    absorb = max(maximum.(abs, (d.u, d.v))...) >= control.threshold

    if absorb || converged || n == control.maxsteps

      d.da .+= control.reg .* log.(d.u)
      d.db .+= control.reg .* log.(d.v)

      absorb!(k, d.da, d.db, control.reg)

      if !converged && n < control.maxsteps

        fill!.((d.u, d.v), 1)
        absorptions += 1

      end
    end
  end

  # Sync the dual buffer
  sync_dual_buffer!(d, control.reg, da = d.da, db = d.db)

  # Trim the kernel if the algorithm converged
  if converged
    trim!(k, control.trim)

  # Print a warning if the algorithm did not converge
  else
    @warn "algorithm `:stable` with regularization " *
          "$(control.reg) failed to converge in $n steps"
  end

  # Get status information about the optimization
  status = ( reg = control.reg
           , steps = n
           , error = err
           , time = dt
           , algorithm = :stable
           , symmetric = control.symmetric
           , converged = converged
           , absorptions = absorptions
           , kernelinfo = kernelinfo(k, control.trim)
           , trimerrorbound = control.trim * length(k) )

  # Return the kernel, the dual buffer, and the status
  k, d, status

end


# ----- Sinkhorn ε-Scaling --------------------------------------------------- #

function scale_subcontrol(c, reg, n, final)

  # Adapt the maximum number of steps
  maxsteps = c.maxsteps - n

  if !final
    tolerance = c.scaletolerance
    error = c.scaleerror
  else
    tolerance = c.tolerance
    error = c.error
  end

  ( c...
  , reg = reg
  , maxsteps = maxsteps
  , tolerance = tolerance
  , error = error )

end

function scale_regularization(reg, control)

  reg = control.scale * reg
  reg <= control.reg ? (control.reg, true) : (reg, false)

end

"""
Implementation of the Sinkhorn algorithm for optimal transport with ε-scaling.
It supports the backend algorithms `_sinkhorn_naive` and `_sinkhorn_stable`.
The vectors `a` and `b` are assumed to be probability vectors. Which part of the
dual buffer is used as input for the algorithm depends on `:algorithm` (see
below). The `control` tuple must contain the entries needed for the specific
`:algorithm` as well as

- `:algorithm`: a symbol that indicates which algorithm to use.
- `:startreg`: initial regularization value.
- `:scale`: scaling factor to decrease the regularization after convergence.
- `:scaletolerance`: `tolerance` if final `reg` value is not reached yet.
- `:scaleerror`: `error` if final `reg` value is not reached yet.

The function returns analog values to `_sinkhorn_naive`, only that the
status also contains the entry `substatus`, which is an array of returned
status tuples by the respective calls of `:algorithm`.

Note that this function assumes compatible types for `a, b, k, d, reg` and
should not be called directly.
"""
function _sinkhorn_scale( a :: AbstractVector
                        , b :: AbstractVector
                        , k :: AbstractKernel
                        , d :: DualBuffer
                        , control )


  # Number of steps and number of scalings
  n = 0
  s = 0

  # Is the next call to `_algorithm` the final one?
  final = false

  # Has the final iteration converged?
  converged = false

  # Did we attempt to sparsify the kernel already?
  sparsified = false

  # Save the status of each scaling step
  substatus = NamedTuple[]

  # Set the initial regularization 
  reg = control.startreg

  # Set the algorithm function
  if control.algorithm == :naive

    _algorithm = _sinkhorn_naive

  elseif control.algorithm == :stable

    _algorithm = _sinkhorn_stable

  else

    error("algorithm '$(control.algorithm)' invalid")

  end

  # Main loop
  dt = @elapsed while n < control.maxsteps && !converged

    subcontrol = scale_subcontrol(control, reg, n, final)

    if n == 0
      @debug("Algorithm :$(control.algorithm) at ε = $reg (n = $n)")
    else
      @debug("Algorithm :$(control.algorithm) at ε = $reg (n = $n)",
             sparsity = status.kernelinfo.sparsity)
    end

    k, d, status = _algorithm(a, b, k, d, subcontrol)

    s += 1
    n += status.steps

    push!(substatus, status)

    if final # The previous application of _algorithm was the final one

      converged = status.converged

    elseif status.converged && n < control.maxsteps

      reg, final = scale_regularization(reg, control)
      # TODO: Should test if holding d.da, d.db constant is really best option
      # Note: sync_dual_buffer is here only required if algorithm == :naive
      # but removing it for algorithm == :stable will probably not yield much
      # performance
      sync_dual_buffer!(d, reg, da = d.da, db = d.db)

      # Note: the kernel values do not have to be updated at this point, since
      # adapting/preparing the kernel is always done during prepare! in _algorithm

      # If the sparsity of the transport plan is high enough, make the kernel
      # sparse.
      if status.kernelinfo.sparsity >= control.sparsify && !sparsified && dense(k)

        @info( "Sparsifying the kernel"
             , type=typeof(k)
             , size=size(k)
             , sparsity=status.kernelinfo.sparsity )

        k = sparsify!(k, control.trim)
        sparsified = true

      end

    end

  end

  # Sync the buffer
  sync_dual_buffer!(d, reg, da = d.da, db = d.db)

  # Get status information about the optimization process
  status = ( reg = reg
           , steps = n
           , error = substatus[end].error
           , time = dt
           , converged = converged
           , scalings = s
           , algorithm = :scale
           , kernelinfo = kernelinfo(k, control.trim)
           , substatus = substatus )

  # Return the kernel, the dual buffer, and the status
  k, d, status

end


# ----- Sinkhorn Multiresolution --------------------------------------------- #

function should_refine(status, control, r)

  status.kernelinfo.sparsity >= control.sharpness[r+1]

end

function coarsen_setup(a, b, d, k, c, partitions)

  p = partitions[1]

  a, b = coarsen(p, a, b)
  d = coarsen(p, d)
  k = coarsen(p, c, k)

  a, b, d, k
end

function refine_setup(a, b, d, k, c, r, partitions)

  # Refine the duals with respect to the current partitions
  p_old = partitions[r]
  d = refine(p_old, d)

  # Coarsen the setup with respect to the new partitions
  p = partitions[r+1]
  a, b = coarsen(p, a, b)
  d = coarsen(p, d)
  k = coarsen(p, c, k, p_old)

  a, b, d, k
end

"""
Implementation of the Sinkhorn algorithm for optimal transport with ε-scaling
and upscaling of resolution.
It supports the algorithms `_sinkhorn_naive` and `_sinkhorn_stable`. The
vectors `a` and `b` are assumed to be probability vectors. Which part of the
dual buffer is used as input for the algorithm depends on `_algorithm`.  The
`control` tuple must contain the entries needed for the specific `_algorithm`,
as well as

- `:algorithm`: a symbol that indicates which algorithm to use.
- `:startreg`: initial regularization value.
- `:scale`: scaling factor to decrease the regularization after convergence.
- `:scaletolerance`: `tolerance` if final `reg` value is not reached yet.
- `:scaleerror`: `error` if final `reg` value is not reached yet.
- `:sharpness`: minimal sharpness that is required of the transport plan before
  increasing the resolution.
- `partitions`: partitionings used for the multiscale approach. 

The function returns analog values to `_sinkhorn_naive`, only that the
status also contains the entry `substatus`, which is an array of returned
status tuples by respective the calls to `_algorithm`.

Note that this function assumes compatible types for `a, b, k, d, reg` and
should usually not be called directly.
"""
function _sinkhorn_multires( a :: AbstractVector
                           , b :: AbstractVector
                           , k :: AbstractKernel  # dummy kernel, only type matters
                           , d :: DualBuffer
                           , control )

  # Number of steps, scalings, and resolution changes
  n = 0
  s = 0
  r = 0

  # Is the next call to `_algorithm` the final one?
  final = false

  # Are we currently at the final refinement?
  final_partition = length(control.partitions) == 1 ? true : false

  # Has the final iteration converged?
  converged = false

  # Did we attempt to sparsify the kernel already?
  sparsified = false

  # Save the status of each scaling step
  substatus = NamedTuple[]

  # Set the initial regularization 
  reg = control.startreg

  # The cost object
  c = control.cost

  # Set the algorithm 
  if control.algorithm == :naive

    _algorithm = _sinkhorn_naive

  elseif control.algorithm == :stable

    _algorithm = _sinkhorn_stable

  else

    error("algorithm '$(control.algorithm)' is invalid")

  end

  # Coarsened variables

  @debug "initial coarsening to partition 1 at regularization $reg"
  a_, b_, d_, k_ = coarsen_setup(a, b, d, k, c, control.partitions)

  # Main loop
  dt = @elapsed while n < control.maxsteps && !converged

    subcontrol = scale_subcontrol(control, reg, n, final)

    if n == 0
      @debug("Algorithm :$(control.algorithm) at ε = $reg (n = $n)")
    else
      @debug("Algorithm :$(control.algorithm) at ε = $reg (n = $n)",
             sparsity = status.kernelinfo.sparsity)
    end

    k_, d_, status = _algorithm(a_, b_, k_, d_, subcontrol)

    n += status.steps

    push!(substatus, status)

    if final # The previous application of _algorithm was the one with minimal reg

      # If we still have steps left and are not at the final partition, let's
      # make the partition smaller and repeat the algorithm with the same value
      # of reg

      while n < control.maxsteps && 
            !final_partition     &&
            should_refine(status, control, r)

        # If the sparsity of the transport plan is high, make the kernel sparse.
        if status.kernelinfo.sparsity >= control.sparsify &&
           !sparsified && dense(k)

          @info( "Sparsifying the kernel"
               , type=typeof(k_)
               , size=size(k_)
               , sparsity=status.kernelinfo.sparsity )

          k_ = sparsify!(k_, control.trim)
          sparsified = true
        end

        r += 1

        @info("Refinement $r at ε = $reg (n = $n)",
              sparsity = status.kernelinfo.sparsity)

        a_, b_, d_, k_ = refine_setup(a, b, d_, k_, c, r, control.partitions)

        final_partition = (r+1 == length(control.partitions))
        
        subcontrol = scale_subcontrol(control, reg, n, final)

        @debug("Algorithm :$(control.algorithm) at ε = $reg (n = $n)",
               sparsity = status.kernelinfo.sparsity)

        k_, d_, status = _algorithm(a_, b_, k_, d_, subcontrol)

        n += status.steps
        push!(substatus, status)

      end

      if status.converged && !final_partition

        @warn "Target regularization $reg is too high to arrive at final " *
              "partitioning under the given sharpness constraints"

      end

      converged = status.converged && final_partition

    elseif status.converged && n < control.maxsteps

      # If the sparsity of the transport plan is high, make the kernel sparse.
      if status.kernelinfo.sparsity >= control.sparsify &&
         !sparsified && dense(k)

        @info( "Sparsifying the kernel"
             , type=typeof(k_)
             , size=size(k_)
             , sparsity=status.kernelinfo.sparsity )

        k_ = sparsify!(k_, control.trim)
        sparsified = true
      end

      # Check whether to refine the setup by going to the next partition. We do
      # so if the kernel's sparsity is higher than the sparsity we would expect
      # if `control.sharpness` elements per row/column in the kernel are
      # non-zero.

      # Either we refine, or we lower the regularization. Doing both
      # can lead to bad consequences, as the refined duals are not as precise
      # as the unrefined ones. This may lead to zero-rows or -columns in the
      # kernel
      if !final_partition && should_refine(status, control, r)

        r += 1

        @info("Refinement $r at ε = $reg (n = $n)",
              sparsity = status.kernelinfo.sparsity)

        a_, b_, d_, k_ = refine_setup(a, b, d_, k_, c, r, control.partitions)

        final_partition = (r+1 == length(control.partitions))

      else

        # Get new, lower regularization parameter 
        reg, final = scale_regularization(reg, control)

        # Count the rescaling
        s += 1

        # And sync the dual buffer with it (Kernel is synced in `_algorithm`)
        sync_dual_buffer!(d_, reg, da = d_.da, db = d_.db)
        end

    end
  end

  # TODO: For scaling and multires algorithms, allow user to set desired
  # final sparsity (instead of regularization)

  # Comments:
  #
  # The algorithm might return kernels / duals that do not correspond to the
  # final partitioning. In particular, this happens if
  #
  # * n == control.maxsteps is reached before the final refinement took place.
  #
  # * we arrive at the final ε, have steps left, but the sparsity condition
  #   is not satisfied (this indicates that the final ε leads to plans
  #   that are so blurry that the finest resolution is not necessary.)

  # Sync the buffer
  sync_dual_buffer!(d_, reg, da = d_.da, db = d_.db)

  # Get status information about the optimization process
  status = ( reg = reg
           , steps = n
           , error = substatus[end].error
           , time = dt
           , converged = converged
           , scalings = s
           , refinements = r
           , algorithm = :multires
           , kernelinfo = kernelinfo(k_, control.trim)
           , substatus = substatus )

  # Return the kernel, the dual buffer, and the status
  k_, d_, status

end

