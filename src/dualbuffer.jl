
# Storage for a collection of variables connected to the dual formulation of the
# optimal transport problem. In particular, these values do *not* have to be in
# sync. Duals are assumed to be dense.

mutable struct DualBuffer{T <: AbstractVector}
  u  :: T
  v  :: T
  da :: T
  db :: T
end

function adapt_type(:: Type{T}, d :: DualBuffer) where {T <: AbstractVector}
  DualBuffer(convert.(T, (d.u, d.v, d.da, d.db))...)
end

# Helper functions
given(args...) = all(!isnothing, args)

function get_arg(T, pairs, key, l)

  if haskey(pairs, key)
    @assert length(pairs[key]) == l
    x = pairs[key]
  else
    x = ones(eltype(T), l)
  end

  convert(T, x)

end

function DualBuffer( T :: Type{<: AbstractArray}
                   , m :: Int
                   , n :: Int
                   ; kwargs... )

  u  = get_arg(T, kwargs, :u, m)
  v  = get_arg(T, kwargs, :v, n)
  da = get_arg(T, kwargs, :da, m)
  db = get_arg(T, kwargs, :db, n)

  DualBuffer(u, v, da, db)

end

function sync_dual_buffer!( d
                          , reg
                          ; u  :: Union{T, Nothing} = nothing
                          , v  :: Union{T, Nothing} = nothing
                          , da :: Union{T, Nothing} = nothing
                          , db :: Union{T, Nothing} = nothing 
                          ) where T <: AbstractVector

  @debug "Syncing the dual buffer at ε = $reg"

  # Either scale variables or dual variables must be given
  if given(u, v)

    d.u .= u
    d.v .= v
    d.da .= reg .* log.(u)
    d.db .= reg .* log.(v)

  elseif given(da, db)

    d.da .= da
    d.db .= db
    d.u  .= exp.(da ./ reg)
    d.v  .= exp.(db ./ reg)

  else

    Base.error("Dual buffer not specified correctly")

  end

  # Return the mutated buffer
  d

end

