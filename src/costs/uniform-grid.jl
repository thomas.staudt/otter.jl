
"""
    UniformGrid{D, F} <: CostMap{F}

Represents the cost matrix for a uniform grid of dimension `D`, array type `T`,
and scalar type `F = eltype(T)`.
"""
struct UniformGrid{D, F} <: CostMap{F}
  shape :: NTuple{D, Int}
  distance
end

"""
    UniformGrid([dt,] shape; [distance])

Create a uniform grid object with optional float type `dt`, shape `shape` and
distance function `distance`. The distance function takes tuples of coordinate
differences `(dx, dy, dz, ...)` and returns a distance value of type `dt`.  By
default, the squared euclidean distance function is used, and it is scaled such
that the longest edge of the grid has length `1`.
"""
function UniformGrid(dt :: Type{<: Real}, shape; distance = nothing)

  dim = length(shape)

  if isnothing(distance)
    m2 = convert(dt, maximum(shape)^2)
    distance = x -> sum(abs2, x) / m2
  end

  UniformGrid{dim, dt}(shape, distance)
end

function UniformGrid(dt :: Type{<: Real}, shape::Int...; kwargs...)
  UniformGrid(dt, shape, kwargs...)
end

UniformGrid(shape...; kwargs...) = UniformGrid(Float64, shape...; kwargs...)

# Basic functionality

Base.size(c :: UniformGrid) = (prod(c.shape), prod(c.shape))
Base.eltype(:: UniformGrid{D, F}) where {D, F} = F

function adapt_type(F :: Type{<: Real}, c :: UniformGrid{D}) where {D}
  UniformGrid{D, F}(c.shape, c.distance)
end

# Realize a uniform grid as matrix
function costdata(c :: UniformGrid{D, F}) where {D, F}
  cost = zeros(F, size(c))
  cind = CartesianIndices(c.shape)
  lind = LinearIndices(cind)

  for i in cind, j in cind
    x = F.(Tuple(i) .- Tuple(j))
    cost[lind[i], lind[j]] = c.distance(x)
  end

  cost
end



# Partitions for Uniform Grids

"""
Partition of a uniform grid in blocks.
"""
struct UniformGridPartition{D, N} <: Partition{N, N}
  gridsize :: NTuple{D, Int}
  partsize :: NTuple{D, Int}
  blocksize :: NTuple{D, Int}
end

function UniformGridPartition( gridsize  :: NTuple{D, Int}
                             , blocksize :: NTuple{D, Int} ) where {D}

  @assert all(gridsize .% blocksize .== 0)

  partsize = div.(gridsize, blocksize)
  UniformGridPartition{D, prod(gridsize)}(gridsize, partsize, blocksize)
end

Base.length(p :: UniformGridPartition) = prod(p.partsize)

# length for a and b in this partitioning
lengths(p) = (prod(p.partsize), prod(p.partsize))

function is_subpartition(p1 :: UniformGridPartition, p2 :: UniformGridPartition)
  (p1.gridsize == p2.gridsize) && all(p2.blocksize .% p1.blocksize .== 0)
end

# Convenience function to generate UniformGridPartitions for given costs

function partition(c :: UniformGrid, p :: UniformGridPartition)
  @assert size(c) = p.gridsize
  p
end

function partition(c :: UniformGrid, p :: Tuple)
  @assert length(p) == length(c.shape)
  @assert all(isinteger, p)
  UniformGridPartition(c.shape, p)
end

function partition(c :: UniformGrid, p :: Integer)
  partition(c, Tuple(p for _ in c.shape))
end

function getpartitions(c, ps)
  pp = map(p -> partition(c, p), ps)
  sub = all(i -> is_subpartition(pp[i+1], pp[i]), 1:(length(pp)-1))
  @assert sub "Given partitions are not proper subpartitions"
  pp
end


## Coarsening functionality 

function coarsen( p :: UniformGridPartition{D, N}
                , a :: Vector
                , b :: Vector
                , f = sum ) where {D, N}

  @assert length(a) == length(b) == N

  # Reshape a to suit the grid
  ra = reshape(a, p.gridsize)
  rb = reshape(b, p.gridsize)

  # CIs for a single block that will be merged/coarsened
  block = CartesianIndices(p.blocksize)

  ca = similar(a, p.partsize)
  cb = similar(b, p.partsize)

  # Obtain the coarsened version of vector a
  for i in CartesianIndices(p.partsize)
    idx = Tuple(i) .- 1
    shifted = block .+ CartesianIndex(idx .* p.blocksize)

    ca[i] = f(j -> ra[j], shifted)
    cb[i] = f(j -> rb[j], shifted)
  end

  reshape(ca, :), reshape(cb, :)
end

function refine( p :: UniformGridPartition{D, N}
               , d :: DualBuffer{T} ) where {D, N, T <: Array}

  @assert prod(p.partsize) == length(d.u)
  @assert prod(p.partsize) == length(d.v)

  du = reshape(d.u, p.partsize)
  dv = reshape(d.v, p.partsize)
  dda = reshape(d.da, p.partsize)
  ddb = reshape(d.db, p.partsize)

  u_ = reshape(repeat(du, inner = p.blocksize), :)
  v_ = reshape(repeat(dv, inner = p.blocksize), :)
  da_ = reshape(repeat(dda, inner = p.blocksize), :)
  db_ = reshape(repeat(ddb, inner = p.blocksize), :)

  DualBuffer(u_, v_, da_, db_)
end

function coarsen( p :: UniformGridPartition{D, N}
                , c :: UniformGrid{D, F} ) where {D, N, F}

  @assert size(c) == (N, N)

  cost = zeros(F, p.partsize..., p.partsize...)
  idx = CartesianIndices(p.partsize)

  for cia in idx, cib in idx

    # Difference tuple in partition-coordinates
    dp = abs.(Tuple(cia) .- Tuple(cib))
    # Minimal difference tuple in grid-coordinates
    dx = max.(0, (dp .- 1) .* p.blocksize .+ 1)

    cost[cia, cib] = c.distance(dx)
  end

  reshape(cost, (length(idx), length(idx)))
end

# Coarsening that only fills the nonzero-entries of a sparse matrix sp
function coarsen!( sp :: SparseMatrixCSC{F}
                 , p :: UniformGridPartition{D, N}
                 , c :: UniformGrid{D, F} ) where {D, N, F}

  @assert size(c)  == (N, N)
  @assert size(sp) == prod.((p.partsize, p.partsize))

  data = nonzeros(sp)
  idx = CartesianIndices(p.partsize)

  r = 0

  for (i, j, _) in zip(findnz(sp)...) 

    r += 1

    dp = abs.(Tuple(idx[i]) .- Tuple(idx[j]))
    dx = max.(0, (dp .- 1) .* p.blocksize .+ 1)

    data[r] = c.distance(dx)
  end

  sp
end

function coarsen( p :: UniformGridPartition{D, N}
                , c :: UniformGrid{D, F}
                , k_old :: DenseKernel{<: Array}
                , p_old = nothing ) where {D, N, F}

  DenseKernel(coarsen(p, c))
end

function coarsen( p :: UniformGridPartition{D, N}
                , c :: UniformGrid{D, F}
                , k_old :: SparseKernel 
                , p_old = nothing ) where {D, N, F}

  @assert size(c)  == (N, N)

  # If no previous partitions are given, we are at the first partitioning,
  # and the given kernel has *not* been coarsened before
  if isnothing(p_old)

    cost = sparse(fill(one(F), prod(p.partsize), prod(p.partsize)))

  # If old partitionings are given, the kernel `k_old` is the previously
  # coarsened one that has to be refined
  else

    @assert all(p_old.blocksize .% p.blocksize .== 0)

    r = div.(p_old.blocksize, p.blocksize)

    shape_old = (prod(p_old.partsize), prod(p_old.partsize))
    shape = (prod(p.partsize), prod(p.partsize))

    @assert size(k_old.cost) == shape_old

    # Prepare the transition from linear to cartesian indices and back (refined)
    cidx = CartesianIndices(p_old.partsize)
    lidx = LinearIndices(p.partsize)

    block = CartesianIndices(r)

    ### Since this part turned out to be a bottleneck when vectorizing with
    ### mapreduce, write it out in imperative style:

    # Initialize counter and coordinate array
    k = 0
    coords = zeros(Int, nnz(k_old.cost)*prod(r)^2, 2)

    # Collect all support coordinates of the refined kernel
    for (i, j, _) in zip(findnz(k_old.cost)...)

      i, j = Tuple(cidx[i]), Tuple(cidx[j])

      i_block = block .+ CartesianIndex((i .- 1) .* r)
      j_block = block .+ CartesianIndex((j .- 1) .* r)

      for ii in lidx[i_block], jj in lidx[j_block]
        k += 1
        coords[k, 1] = ii
        coords[k, 2] = jj
      end

    end

    # Create the refined sparse cost matrix from the indices
    cost = sparse(coords[:,1], coords[:,2], one(F), shape...)
  end

  # Fill the refined sparse cost matrix using the cost object
  cost = coarsen!(cost, p, c)

  # Create the kernel
  data = similar(cost)

  SparseKernel(cost, data)
end

