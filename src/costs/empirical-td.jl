
struct ETD{F} <: CostMap{F}
  cx :: Matrix{F}
  cy :: Matrix{F}
  link
end

function ETD( dt :: Type{<: Real}, cx :: Matrix, cy :: Matrix
            ; link = (cx, cy) -> cx + cy )

  @assert size(cx, 1) == size(cx, 2)
  @assert size(cx) = size(cy)

  ETD(convert(Array{dt}, cx), convert(Array{dt}, cy), link)
end

function ETD( dt :: Type{<: Real}, xs :: Vector, ys :: Vector
            ; costx = (x1, x2) -> norm(x1 - x2)
            , costy = (y1, y2) -> norm(y1 - y2)
            , link = (cx, cy) -> cx + cy )

  @assert length(xs) == length(ys)

  # construct cost matrices of spaces X and Y
  cx = [dt(costx(x1, x2)) for x1 in xs, x2 in xs]
  cy = [dt(costy(y1, y2)) for y1 in ys, y2 in ys]

  ETD(cx, cy, link)
end

ETD(xs :: Array, ys :: Array; kwargs...) = ETD(Float64, xs, ys; kwargs...)

Base.size(c :: ETD) = (size(c.cx, 1), prod(size(c.cx)))
Base.eltype(:: ETD{F}) where {F} = F

function adapt_type(F :: Type{<: Real}, c :: ETD)
  cx = convert(Matrix{F}, c.cx)
  cy = convert(Matrix{F}, c.cy)
  ETD(cx, cy, c.link)
end

function costdata(c :: ETD{F}) where {F}
  n = size(c, 1)
  I = 1:n
  data = c.link.(cx[I, I], cy[I, I'])
  reshape(data, n, :)
end

struct ETDPartition{N, M} <: Partition{N, M}
  px :: Vector{Vector{Int}} # expect ordered groups
  py :: Vector{Vector{Int}} # expect ordered groups
end

Base.length(p :: ETDPartition) = length(px) * length(py)

function is_subpartition(p1 :: ETDPartition, p2 :: ETDPartition)
  sub = true
  for group1 in p1.px, group2 in p2.px
    sub &= isempty(intersect(group1, group2)) || issubset(group1, group2)
  end
  for group1 in p1.py, group2 in p2.py
    sub &= isempty(intersect(group1, group2)) || issubset(group1, group2)
  end
  sub
end

# MISSING: convenience functions to create partitions...
#function partition(...)
#end

function coarsen( p :: ETDPartition
                , a :: Vector
                , b :: Vector
                , f = sum )

  # First implementation attempt: only coarsen b
  rb = reshape(b, (length(a), length(a)))
  cb = similar(b, (length(p.px), length(p.py)))
  for i in 1:length(p.px), j in 1:length(p.py)
    cb[i, j] = f(rb[p.px[i], p.py[j]]) # TODO: this could be slow, benchmark!
  end

  a, reshape(cb, :)
end

function refine( p :: ETDPartition{N, M}
               , d :: DualBuffer{T}) where {N, M, T <: Array}

  @assert N*N == M == length(p) == length(d.v)

  v  = reshape(d.v, length(p.px), length(p.py))
  v_ = similar(v, (N, N))

  db  = reshape(d.db, length(p.px), length(p.py))
  db_ = similar(db, (N, N))

  for i in 1:length(p.px), j in 1:length(p.py)
    v_[p.px[i], p.py[j]]  .= v[i, j]
    db_[p.px[i], p.py[j]] .= db[i, j]
  end

  DualBuffer(d.u, reshape(v_, M), d.da, reshape(db_, M))
end

function coarsen(p :: ETDPartition{N, M}, c :: ETD{F}) where {N, M, F}

  cost = zeros(F, N, length(p.px), length(p.py))

  for i in 1:N, jx in 1:length(p.px), jy in 1:length(p.py)
    cx = minimum(ix -> c.cx[i, ix], p.px[jx])
    cy = minimum(iy -> c.cy[i, iy], p.py[jy])
    # Here, it is crucial that the link function is monotonous.
    # Otherwise, we could have to iterate over the whole product of
    # p.px and p.py, which could take much longer
    cost[i, jx, jy] = c.link(cx, cy)
  end

  reshape(cost, N, :)
end

function coarsen!( sp :: SparseMatrixCSC{F}
                 , p :: ETDPartition{N, M}
                 , c :: ETD{F} ) where {F, N, M}

  @assert size(c) == (N, M)
  @assert size(sp) == (N, length(p.px) * length(p.py))

  data = nonzeros(sp)
  idx = CartesianIndices(length(p.px), length(p.py))

  r = 0
  for (i, j, _) in zip(findnz(sp)...)

    r += 1
    jx, jy = Tuple(idx[j])

    cx = minimum(ix -> c.cx[i, ix], p.px[jx])
    cy = minimum(iy -> c.cy[i, iy], p.py[jy])
    data[r] = c.link(cx, cy)
  end

  sp
end

function coarsen( p :: ETDPartition
                , c :: ETD
                , k_old :: DenseKernel{<: Array}
                , p_old = nothing ) where {D, N, F}

  DenseKernel(coarsen(p, c))
end

function coarsen( p :: ETDPartition{N, M}
                , c :: ETD{F}
                , k_old :: SparseKernel
                , p_old = nothing ) where {N, M, F}



end
