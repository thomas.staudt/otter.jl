
#
# Otter.jl
#
# Optimal TransporT with Entropic Regularization in the julia programming
# language
# 

"""
Otter - Optimal TransporT with Entropic Regularization. Provides several
efficient implementations of the Sinkhorn algorithm for solving
entropy-regularized optimal transport problems. 
"""
module Otter

  import Logging
  using LinearAlgebra, SparseArrays, Statistics

  using Requires # handling optional CUDA dependency

  include("dualbuffer.jl")
  include("kernel.jl")
  include("cost.jl")

  export UniformGrid

  include("algorithms.jl")
  include("api.jl")

  export sinkhorn_naive, sinkhorn_stable, 
         sinkhorn_scale, sinkhorn_multires, 
         kernel, gradients, status, substatus,
         plan, cost, converged, regularization,
         steps, runtime, kernelinfo, sparsity

  """
  A collection of miscellaneous auxiliary functions and methods used to profile
  and test the Otter package.
  """
  module Otoys
    using ..Otter
    include("otoys.jl")
  end

  export Otoys

  include("util.jl")

  # conditional loading of CUDA glue code
  function __init__()
    @require CUDA="052768ef-5323-5732-b1bb-66c8b64840ba" include("cuda.jl")
  end

end

