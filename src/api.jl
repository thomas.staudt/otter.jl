
#
# This file implements high-level functions with syntax
# ```
# sinkhorn_*(a, b, c; reg, kwargs...)
# ```
# where `*` denotes the choice of an algorithm (`naive`, `scale`, `multires`).
# The arguments `a` and `b` are probability vectors between the transport plan
# shall be calculated, while `c` is a cost matrix or a cost function. The
# keyword arguments depend on the method `*`.
# 
# Application of the methods returns a `Return` object `ret` that contains the
# obtained kernel / dual variables and holds information about the respective
# Sinkhorn iteration. The return object can be used to calculate the Sinkhorn
# cost `cost(ret)`, the transport plan `plan(ret)`, and gradients
# `gradients(ret)`. Further information is collected in the status
# `status(ret)`, which is a named tuple with meta-information, like the number
# of steps or whether the algorithm has converged (according to the specified
# `error` and `tolerance`).
#

# Common result structure for all algorithms

"""
Contains results and meta-information about a Sinkhorn optimization run.
"""
struct Return
  kernel
  dualbuffer
  status
end

# conversion function that is applied on array-valued objects that can be
# retrieved from Return
_return(a :: AbstractArray) = a

#
# Convenience functions to obtain information from Return
#

"""
    kernel(ret :: Return)

Get the kernel object returned by the Sinkhorn algorithm.
"""
kernel(ret :: Return) = ret.kernel


"""
    gradients(ret :: Return)

Get gradients `(da, db)` of the input measures `a` and `b` with respect to the
Sinkhorn cost.
"""
function gradients(ret :: Return)
  _return(ret.dualbuffer.da), _return(ret.dualbuffer.db)
end


"""
    status(ret :: Return)

Get a status tuple that contains metainformation about the optimization.  Its
entries depend on the algorithm used; see the respective help information.
"""
status(ret :: Return) = ret.status

"""
    substatus(ret :: Return)

Get the vector of status tuples that belong to different stages of the
optimization.
"""
substatus(ret :: Return) = ret.status.substatus


"""
    plan(ret :: Return)

The transport plan yielded by the optimization. Its format as a matrix can be
dense or sparse, depending on the employed kernel.
"""
plan(ret :: Return) = _return(_plan(ret.kernel))


"""
    cost(ret :: Return)

The Sinkhorn cost.
"""
cost(ret :: Return) = _cost(ret.kernel)


"""
    converged(ret :: Return)

Check whether the convergence criterion has been satisfied or not.
"""
converged(ret :: Return) = ret.status.converged


"""
    regularization(ret :: Return)

The (final) regularization parameter.
"""
regularization(ret :: Return) = ret.status.reg


"""
    steps(ret :: Return)

The total number of Sinkhorn scaling steps.
"""
steps(ret :: Return) = ret.status.steps


"""
    error(ret :: Return)

Error value at the end of the optimization.
"""
error(ret :: Return) = ret.status.error


"""
    runtime(ret :: Return)

Time (in seconds) spent in the Sinkhorn steps.
"""
runtime(ret :: Return) = ret.status.time

"""
    kernelinfo(ret :: Return)

Information about the kernel returned by the Sinkhorn algorithm.
"""
kernelinfo(ret :: Return) = ret.status.kernelinfo

"""
    sparsity(ret :: Return)

Sparsity of the kernel returned by the Sinkhorn algorithm.
"""
sparsity(ret :: Return) = ret.status.kernelinfo.sparsity


#
# Sinkhorn algorithms with user-friendly interfaces
# The actual implementations can be found in algorithm.jl
#

# consistency checks and input pre-processing

const _atype = Dict(
    "cpu"   => Array{Float64}
  , "cpu64" => Array{Float64}
  , "cpu32" => Array{Float32}
)

"""
    check_input(a, b, c)

Checks if `a` and `b` are in the probability simplex and if their
sizes are compatible to the costs `c`.
"""
function check_input(a, b, c)
  @assert sum(a) ≈ 1. && sum(b) ≈ 1.
  m, n = length(a), length(b)
  @assert size(c) == (m, n)

  m, n
end


"""
    prepare_input(atype, scalars, vectors, cost)

Force a list of `scalars`, a list of `vectors`, and a `cost` matrix to be
consistent with the array type `atype`.
"""
function prepare_input(atype, scalars, vectors, cost)

  # if atype is not provided, try to infer it from the given vectors
  if isnothing(atype) || atype == ""

    vtype = typejoin(typeof.(vectors)...)
    t = findfirst(x -> vtype <: x, _atype)
    if isnothing(t)
      @warn "Cannot infer compatible array types. Falling back to `\"cpu64\"`"
      atype = Array{Float64}
    else
      atype = _atype[t]
    end

  # if atype is a string, look up the actual type in _atype
  elseif isa(atype, String)
    if haskey(_atype, atype)
      atype = _atype[atype]
    else
      @warn "Option atype = \"$atype\" is invalid. Falling back to atype = \"cpu64\""
      atype = Array{Float64}
    end
  end

  cost = adapt_type(atype, cost)
  scalars = convert.(eltype(atype), scalars)
  vectors = convert.(atype, vectors)

  if any(!isfinite, scalars)
    @warn "Non-finite scalars after initial type conversion" scalars
  end

  atype, scalars, vectors, cost
end


# Naive Sinkhorn

function prepare_naive( a :: AbstractVector
                      , b :: AbstractVector
                      , c :: Cost
                      ; reg :: Real
                      , tol :: Real = 1e-5
                      , maxsteps :: Int = 10000
                      , interval :: Int = 10
                      , vinit :: AbstractVector = fill!(similar(b), 1)
                      , kernel :: Type{<: AbstractKernel} = DenseKernel
                      , error = relerror
                      , atype = ""
                      , trim :: Real = -1.
                      , symmetric :: Bool = false
                      , _dummykernel :: Bool = false
                      , kernelargs... )

  @assert reg > 0
  @assert tol > 0
  @assert maxsteps > 0
  @assert interval > 0

  m, n = check_input(a, b, c)
  atype, (reg, trim), (a, b, init), c = prepare_input( atype
                                                     , [reg, trim]
                                                     , [a, b, vinit], c)

  k = kernel(c; _dummy = _dummykernel, atype = atype, kernelargs...)
  d = DualBuffer(atype, m, n, v = vinit)

  control = ( reg = reg
            , error = error
            , maxsteps = maxsteps
            , interval = interval
            , tolerance = tol
            , trim = trim
            , symmetric = symmetric )

  a, b, k, d, control

end  

"""
    sinkhorn_naive(a, b, c; <keyword arguments>)

Perform the Sinkhorn scaling algorithm on probability measures `a` and `b` with
cost matrix `c`. No special numerical stability considerations are applied.

# Arguments
- `reg :: Real`: entropy regularization parameter.
- `tol :: Real`: error tolerance to accept convergence.
- `maxsteps :: Int`: maximal number of Sinkhorn steps.
- `interval :: Int`: check for convergence after every `interval` steps
- `trim :: Real`: values < `trim` are considered to be zero for sparsity calculations.
- `symmetric :: Bool`: enable symmetric sinkhorn steps.
- `vinit :: AbstractVector`: initial value for the scale variable `v`.
- `kernel :: Type{<: AbstractKernel}`: kernel to be used in the algorithm.
- `atype`: array type that inputs are converted to (may be a string like "cpu64").
- `error`: error function used to estimate the convergence.
- `debug :: Bool`: enable debugging messages.
- `quiet :: Bool`: suppress all log messages, overwrites `debug = true`.
- `kernelargs...`: Keyword arguments used to construct the kernel.

# Examples
```julia
a, b = rand(10), rand(10)
c = [(i-j)^2 for i in 1:10, j in 1:10]
r = sinkhorn_naive(a, b, c, reg = 0.25, maxsteps = 100, tol = 1e-3)

plan(r) # transport plan
cost(r) # sinkhorn cost
```
"""
function sinkhorn_naive(args...; debug = false, quiet = false, kwargs...)

  with_logging(debug, quiet) do

    a, b, k, d, control = prepare_naive(args...; kwargs...)
    k, d, s = _sinkhorn_naive(a, b, k, d, control)

    Return(k, d, s)

  end
end


# Stable implementation

function prepare_stable( a :: AbstractVector
                       , b :: AbstractVector
                       , c :: Cost
                       ; reg :: Real
                       , maxsteps :: Int = 10000
                       , interval :: Int = 10
                       , tol :: Real = 1e-5
                       , dainit :: AbstractVector = zeros(length(a))
                       , dbinit :: AbstractVector = zeros(length(b))
                       , kernel :: Type{<: AbstractKernel} = DenseKernel
                       , threshold :: Real = 1e20
                       , symmetric :: Bool = false
                       , error = relerror
                       , atype = ""
                       , trim :: Real = -1.
                       , _dummykernel :: Bool = false
                       , kernelargs... )

  @assert reg > 0
  @assert tol > 0
  @assert maxsteps > 0
  @assert interval > 0
  @assert threshold > 0

  m, n = check_input(a, b, c)
  atype, scalars, vectors, c = prepare_input( atype
                                            , [reg, trim, threshold]
                                            , [a, b, dainit, dbinit], c )
  (reg, trim, threshold) = scalars
  (a, b, dainit, dbinit) = vectors
  
  k = kernel(c; _dummy = _dummykernel, atype = atype, kernelargs...)
  d = DualBuffer(atype, m, n, da = dainit, db = dbinit)

  control = ( reg = reg
            , error = error
            , maxsteps = maxsteps
            , interval = interval
            , tolerance = tol
            , trim = trim
            , threshold = threshold
            , symmetric = symmetric )

  a, b, k, d, control

end

"""
    sinkhorn_stable(a, b, c; <keyword arguments>)

Perform the Sinkhorn scaling algorithm on probability measures `a` and `b` with
cost matrix `c`. Partial log-stabilization, i.e., absorption of the scale
variables in the kernel, is applied.

# Arguments
- `reg :: Real`: entropy regularization parameter.
- `tol :: Real`: error tolerance to accept convergence.
- `maxsteps :: Int`: maximal number of Sinkhorn steps.
- `interval :: Int`: check for convergence after every `interval` steps.
- `threshold :: Real`: absorption threshold for the maximal entries in `u`, `v`.
- `symmetric :: Bool`: enable symmetric sinkhorn steps.
- `trim :: Real`: values < `trim` are considered to be zero for sparsity calculations.
- `vinit :: AbstractVector`: initial value for the scale variable `v`.
- `kernel :: Type{<: AbstractKernel}`: kernel to be used in the algorithm.
- `atype`: array type that inputs are converted to (may be a string like "cpu64").
- `error`: error function used to estimate the convergence.
- `debug :: Bool`: enable debugging messages.
- `quiet :: Bool`: suppress all log messages, overwrites `debug = true`.
- `kernelargs...`: Arguments used to construct the kernel.

# Examples
```julia
a, b = rand(10), rand(10)
c = [(i-j)^2 for i in 1:10, j in 1:10]
r = sinkhorn_stable(a, b, c, reg = 0.25, maxsteps = 100, threshold = 1e25)

plan(r) # transport plan
cost(r) # sinkhorn cost
```
"""
function sinkhorn_stable(args...; debug = false, quiet = false, kwargs...)

  with_logging(debug, quiet) do

    vars = prepare_stable(args...; kwargs...)
    k, d, s = _sinkhorn_stable(vars...)

    Return(k, d, s)

  end
end


"""
    sinkhorn_scale(a, b, c; algorithm = :stable, <keyword arguments>)

Perform the Sinkhorn scaling algorithm on probability measures `a` and `b` with
cost matrix `c`. The regularization parameter is scaled down during the
iterations.

# Arguments
The function takes all keyword arguments that `sinkhorn_naive` or
`sinkhorn_stable` take if `algorithm = :naive` or `algorithm = :stable`,
respectively. Additionally, it accepts
- `startreg :: Real`: initial entropy regularization parameter.
- `scalesteps :: Int`: number of scalings. can be provided instead of `startreg`.
- `scale :: Real`: scaling factor for the regularization parameter.
- `sparsify :: Real`: value between 0 and 1 that indicates from which sparsity
in the transport plan on a sparse kernel is to be used.
- `scaletol :: Real`: `tol` if final value of `reg` is not reached yet.
- `scaleerror`: `error` if final value of `reg` is not reached yet.

# Examples
```julia
a, b = rand(10), rand(10)
c = [(i-j)^2 for i in 1:10, j in 1:10]
r = sinkhorn_scale(a, b, c, reg = 0.01, 
                   startreg = 1., scale = 0.75, maxsteps = 250)

plan(r) # transport plan
cost(r) # sinkhorn cost
```
"""
function sinkhorn_scale( args...
                       ; reg :: Real
                       , startreg :: Real = 0
                       , scalesteps :: Int = 6
                       , scale :: Real = 0.5
                       , scaletol :: Real = -1
                       , scaleerror = nothing
                       , algorithm :: Symbol = :stable
                       , sparsify :: Real = 0.8
                       , debug = false
                       , quiet = false
                       , kwargs... )

  with_logging(debug, quiet) do

    # Sanity checks for scalars

    @assert 0 < sparsify < 1
    @assert 0 < scale < 1
    @assert 0 <= scalesteps

    # Convert scalesteps to startreg, if latter is not provided

    if startreg < reg

      startreg = reg / scale^scalesteps

    end

    # Prepare the input for the respective algorithms

    if algorithm == :naive

      a, b, k, d, control = prepare_naive(args...; reg = reg, kwargs...)

    elseif algorithm == :stable

      a, b, k, d, control = prepare_stable(args...; reg = reg, kwargs...)

    else

      error("algorithm $algorithm not supported")

    end

    # Append the additional control information needed by _sinkhorn_scale

    ft       = typeof(control.reg)
    startreg = convert(ft, startreg)
    scale    = convert(ft, scale)
    sparsify = convert(ft, sparsify)

    scaletol < 0 && (scaletol = control.tolerance)
    isnothing(scaleerror) && (scaleerror = control.error)

    control = ( control...
              , startreg = startreg
              , scale = scale
              , scaletolerance = scaletol
              , scaleerror = scaleerror
              , sparsify = sparsify
              , algorithm = algorithm )

    # Call the alogrithm and return the result object

    k, d, s = _sinkhorn_scale(a, b, k, d, control)

    Return(k, d, s)

  end
end


## Auxiliary functions for sinkhorn_multires

function sharpness_bounds(partitions, sharpness :: Int)
  map(partitions[1:end-1]) do p
    1 - sharpness / max(lengths(p)...)
  end
end

function sharpness_bounds(partitions, sharpness)
  @assert length(sharpness) == (length(partitions) - 1) "wrong number of sparsity bounds"
  @assert all(0. .<= sharpness .<= 1.) "sparsity bounds not between 0 and 1"
  @assert all(sort(sharpness) .== sharpness) "sparsity bounds must be increasing"

  sharpness
end


"""
    sinkhorn_multires(a, b, c; algorithm = :stable, <keyword arguments>)

Perform the Sinkhorn scaling algorithm on probability measures `a` and `b` with
cost matrix `c`. The regularization parameter is scaled down during the iterations
while the resolution of the transport plan is increased according to a series
of partitionings.

**Note:** Currently, multiresolution Sinkhorn scaling only works for costs
of type `UniformGrid`. Also, the choice when to upscale the problem currently
depends on the sparsity of the transport plan. In situations where non-sparse
plans are expected, this could be a poor choice.

# Arguments
The function takes all keyword arguments that `sinkhorn_naive` or
`sinkhorn_stable` take if `algorithm = :naive` or `algorithm = :stable`,
respectively. Additionally, it accepts
- `startreg :: Real`: initial entropy regularization parameter.
- `scalesteps :: Int`: number of scalings. can be provided instead of `startreg`.
- `sparsify :: Real`: value between 0 and 1 that indicates at which sparsity 
  a sparse kernel is used.
- `scale :: Real`: scaling factor for the regularization parameter.
- `scaletol :: Real`: `tol` if final `reg` is not reached yet.
- `scaleerror`: `error` if final `reg` is not reached yet.
- `partitions`: `Partition`s to use when increasing resolution. For costs `c`
  of type `UniformGrid` it can also be a list of integers, like `[4, 2, 1]`,
  which is interpreted as a series of block-partitions that group 4, 2, or
  1 element in each dimension of the grid.
- `sharpness`: Sharpness criterion that the transport plan must satisfy before
  the resolution is increased. If it is an integer, it signifies the average
  number of nonzero entries per row/column in the transport plan. Otherwise, it
  has to be a list of length `length(partitions) - 1` with increasing sparsity
  values between 0 and 1.

If the argument `partitions` is not given, the algorithm will behave like
`sinkhorn_scale` and no refinement of resolutions is applied.

# Examples
```julia
a, b = rand(1024), rand(1024)
c = UniformGrid((32, 32))
r = sinkhorn_multires(a, b, c, reg = 1e-4, startreg = 0.1, 
                      tol = 1e-4, scale = 0.5, partitions = [4, 2, 1], 
                      sharpness = 25, trim = 1e-20)

plan(r) # transport plan
cost(r) # sinkhorn cost
```
"""
function sinkhorn_multires( a, b, c :: CostMap
                          ; reg :: Real
                          , startreg :: Real = 0
                          , scalesteps :: Int = 10
                          , scale :: Real = 0.5
                          , scaletol :: Real = NaN
                          , scaleerror = nothing
                          , algorithm :: Symbol = :stable
                          , sparsify :: Real = 0.9
                          , sharpness = 25
                          , partitions = nothing
                          , debug = false
                          , quiet = false
                          , kwargs... )

  with_logging(debug, quiet) do

    # Sanity checks for scalars

    @assert 0 < sparsify < 1
    @assert 0 < scale < 1
    @assert 0 < scaletol
    @assert 0 <= scalesteps
    @assert 0 < sharpness

    # Convert scalesteps to startreg, if latter is not provided

    if startreg < reg

      startreg = reg / scale^scalesteps

    end

    # Prepare the partitions object

    partitions = getpartitions(c, partitions)
    sharpness = sharpness_bounds(partitions, sharpness)

    @info "Refinements planned for sparsities $sharpness" 

    # Prepare the input for the respective algorithms

    if algorithm == :naive

      a, b, k, d, control = prepare_naive(a, b, c; reg = reg, 
                                          _dummykernel = true, kwargs...)

    elseif algorithm == :stable

      a, b, k, d, control = prepare_stable(a, b, c; reg = reg, 
                                           _dummykernel = true, kwargs...)

    end

    # Append the additional control information needed by _sinkhorn_scale

    ft       = typeof(control.reg)
    startreg = convert(ft, startreg)
    scale    = convert(ft, scale)
    sparsify = convert(ft, sparsify)

    !isfinite(scaletol)   && (scaletol = control.tolerance)
    isnothing(scaleerror) && (scaleerror = control.error)

    control = ( control...
              , cost = adapt_type(ft, c)
              , sharpness = sharpness
              , partitions = partitions
              , startreg = startreg
              , scale = scale
              , scaletolerance = scaletol
              , scaleerror = scaleerror
              , sparsify = sparsify
              , algorithm = algorithm )

    # Call the alogrithm and return the result object

    k, d, s = _sinkhorn_multires(a, b, k, d, control)

    Return(k, d, s)

  end
end


