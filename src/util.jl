

# Default relative error function as convergence criterion on the scale
# variables
function relerror(u, u_old, v, v_old)

  mu = maximum((u .- u_old) ./ u)
  mv = maximum((v .- v_old) ./ v)
  max(mu, mv)

end


# Auxiliary function for locally activating different loglevels
function with_logging(f :: Function, debug, quiet)

  if quiet
    logger = Logging.NullLogger()
  elseif debug
    logger = Logging.ConsoleLogger(stderr, Logging.Debug)
  else
    logger = Logging.ConsoleLogger(stderr, Logging.Info)
  end

  Logging.with_logger(f, logger)

end

