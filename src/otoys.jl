
using LinearAlgebra

function image( m, n, f = (_,_) -> 1.
              ; box_size = (1., 1.)
              , noise = () -> 0. )

  img = map(CartesianIndices((1:m, 1:n))) do ci
    i, j = Tuple(ci)
    y, x = box_size .* ((i-1) / (m-1), (j-1) / (n-1))
    f(y, x) + noise()
  end

  img = reshape(img, :)
  normalize!(img .- min(0, minimum(img)), 1)

end

function cost2d(m, n, norm = 2; power = 1., max_entry = 1.)

  c = map(CartesianIndices((m, n, m, n))) do ci
    y1, x1, y2, x2 = Tuple(ci)

    ((y1 - y2)^norm + (x1 - x2)^norm)^power
  end

  c = reshape(c, m*n, m*n)
  c / maximum(c) * max_entry

end


"""
Gradient descent along the Sinkhorn distance from a to b with cost c.
"""

project_simplex!(a, eps = 1e-50) = normalize(clamp!(a, eps, Inf), 1)

function descent(a, b, c; stepwidth = 1e-2, steps = 1, eps = 1e-50, kwargs...)

  for i in steps

    a .= a .- stepwidth * gradients(sinkhorn(a, b, c; kwargs...))[1]
    project_simplex!(a)

  end

  a

end


