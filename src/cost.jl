
"""
    CostMap{F}

Type that represents a cost matrix with elements of type `F` in a
memory-efficient way. It can be queried for selected parts of the cost matrix
and should calculate them on the fly. Examples are the `UniformGrid`.
"""
abstract type CostMap{F} end

Base.eltype(:: CostMap{F}) where {F} = F

"""
    adapt_type(T, cost)
    adapt_type(F, cost)

Adapt the cost object `cost` to be compatible with array type `T` or float type
`F`.
"""
function adapt_type(T :: Type{<: AbstractArray}, c :: CostMap)
  # when called with array type T, try to use implementation for element types
  # as fallback
  adapt_type(eltype(T), c)
end

"""
    Cost{F} = Union{AbstractMatrix{F}, CostMap{F}}

Abstract cost type that can be a matrix with elements of type `F` or
a `CostMap{F}`.
"""
const Cost{F} = Union{AbstractMatrix{F}, CostMap{F}} where {T <: Real}

"""
    Partition{N, M}

Abstract partition type that stands for the division of a space with `N` and
a space with `M` elements into groups. Usually, a concrete subtype of
`Partition` will be closely related to an implementation of type `CostMap`.
"""
abstract type Partition{N, M} end

"""
    partition(costmap, args...)

Convenience function to construct partitions that work under the cost map
`costmap`.
"""
function partition end

# Fallback implementation for coarsening and refining.
# Used for array types that have no specialized versions of the following calls

"""
    coarsen(partition, dualbuffer)

Coarsens the dual variables `u`, `v`, `da`, and `db` of `dualbuffer` according
to `partition`.
"""
function coarsen( p :: Partition{N, M}
                , d :: DualBuffer ) where {N, M}

  @assert length(d.u) == N
  @assert length(d.v) == M

  u, v = coarsen(p, d.u, d.v, maximum)
  da, db = coarsen(p, d.da, d.db, maximum)

  DualBuffer(u, v, da, db)
end 

"""
    coarsen(partition, a, b, f = sum)

Coarsens the vectors `a` and `b` according to `partition` with operation `f`.
"""
function coarsen( p :: Partition
                , a :: AbstractVector
                , b :: AbstractVector
                , args... )

  a_ = convert(Vector, a)
  b_ = convert(Vector, b)

  a_, b_ = coarsen(p, a_, b_, args...)

  convert(typeof(a), a_), convert(typeof(b), b_)
end

"""
    coarsen(partition, cost, kernel, old_partition = nothing)

Produces a new kernel from `kernel` that is coarsened according to `partition`.
The argument `cost` should be a `CostMap`. If `old_partition` is not provided,
it is assumed that this is the first partitioning and the (potential) sparsity
of `kernel` is not transfered to the new kernel.
"""
function coarsen( p :: Partition
                , c :: Cost
                , k :: DenseKernel{T}
                , args... ) where {T}

  k_ = coarsen(p, c, adapt_type(Array, k), args...)
  adapt_type(T, k_)
end

function coarsen( p :: Partition
                , c :: Cost
                , k :: AbstractSparseKernel
                , args... )

  k_ = coarsen(p, c, convert(SparseKernel, k), args...)
  convert(typeof(k), k_)
end

"""
    refine(partition, dualbuffer)

Refine the dual buffer `dualbuffer` to full resolution, assuming that is was
calculated under `partition`.
"""
function refine( p :: Partition
               , d :: DualBuffer{T} ) where {T}

  d_ = refine(p, adapt_type(Vector, d))
  adapt_type(T, d_)
end


# ----- Cost matrices -------------------------------------------------------- #

adapt_type(T :: Type{<: AbstractArray}, c :: AbstractMatrix) = convert(T, c)

# This method of adapt_type should only be called by sinkhorn_multiscale, which
# does not support cost matrices. Therefore, we do not have to implement it.
function adapt_type(F :: Type{<: Real}, c :: AbstractMatrix)
  error("adapt_type with floating point argument $F not implemented")
end

function adapt_type(T :: Type{<: Array}, c :: SparseMatrixCSC)
  convert(SparseMatrixCSC{eltype(T)}, c)
end

costdata(c :: AbstractMatrix) = c


# ----- Cost implementations ------------------------------------------------- #

module Costs

using SparseArrays

import ..Otter: Cost, CostMap, Partition, DualBuffer,
                AbstractKernel, AbstractSparseKernel,
                DenseKernel, SparseKernel

import ..Otter: adapt_type, costdata, partition

include("costs/uniform-grid.jl")

include("costs/empirical-td.jl")

export UniformGrid, ETD, partition

end # module Costs


