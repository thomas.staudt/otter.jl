
# signal that conditional loading was successful
@info "Detected CUDA support"

# import CUDA functionality. By the merits of Requires.jl, we can import it from
# the Otter module (via .CUDA instead of CUDA). This prevents "missing
# dependency" warnings
using .CUDA
import .CUDA.CUSPARSE: CuSparseMatrixCSR, CuSparseMatrixCOO

# register new valid string arguments for 'atype' when calling a flavour of the
# sinkhorn_* functions
push!( _atype
     , "gpu"   => CuArray{Float32}
     , "gpu32" => CuArray{Float32}
     , "gpu64" => CuArray{Float64} )


# conversion of CuArray typed values to standard julia types
_return(v :: CuArray) = Array(v)
_return(v :: CuSparseMatrixCSR) = SparseMatrixCSC(v)

## CUDA Powered Sparse Kernels

"""
Sparse kernel based on `CuSparseMatrixCSR` of the `CUDA` package. These kernels
are generated when kernels of type `DenseKernel{<: CuArray}` are sparsified.
"""
mutable struct CuSparseKernel{F} <: AbstractSparseKernel
  cost :: CuSparseMatrixCSR{F}
  data :: CuSparseMatrixCSR{F}
end

# Conversion functions that can be used for fallback functionality with no
# specialized implementation for CuSparseKernels
function Base.convert(:: Type{SparseKernel}, k :: CuSparseKernel{F}) where {F}
  SparseKernel(convert(SparseMatrixCSC, k.cost), convert(SparseMatrixCSC, k.data))
end

function Base.convert(:: Type{SparseKernel{F}}, k :: CuSparseKernel{F}) where {F}
  SparseKernel(convert(SparseMatrixCSC, k.cost), convert(SparseMatrixCSC, k.data))
end

function Base.convert(:: Type{CuSparseKernel}, k :: SparseKernel{F}) where {F}
  CuSparseKernel(CuSparseMatrixCSR{F}(k.cost), CuSparseMatrixCSR{F}(k.data))
end

function Base.convert(:: Type{CuSparseKernel{F}}, k :: SparseKernel{F}) where {F}
  CuSparseKernel(CuSparseMatrixCSR{F}(k.cost), CuSparseMatrixCSR{F}(k.data))
end

function Base.copyto!(cuk :: CuSparseKernel{F}, k :: SparseKernel{F}) where {F}
  cuk.cost = CuSparseMatrixCSR{F}(k.cost)
  cuk.data = CuSparseMatrixCSR{F}(k.data)
end

costdata(k :: CuSparseKernel) = k.cost
kerneldata(k :: CuSparseKernel) = k.data

_plan(k :: CuSparseKernel) = k.data
_cost(k :: CuSparseKernel) = dot(nonzeros(k.cost), nonzeros(k.data))

# A common idea in the following code: use CuSparseMatrixCOO as intermediate
# format, since index positions can easily be understood in it

# TODO: can we do trim! with more in-place operations?
function _trim!(k :: CuSparseKernel{F}, threshold) where {F}

  nk = nonzeros(k.data)

  # Boolean mask of all values we want to keep
  sel = nk .> threshold

  # trim the data matrix
  dcoo = CuSparseMatrixCOO(k.data)
  dtrim = CuSparseMatrixCOO{F}(dcoo.rowInd[sel], dcoo.colInd[sel], dcoo.nzVal[sel])

  # trim the cost matrix
  ccoo = CuSparseMatrixCOO(k.cost)
  ctrim = CuSparseMatrixCOO{F}(ccoo.rowInd[sel], ccoo.colInd[sel], ccoo.nzVal[sel])

  # modify the kernel
  k.cost = CuSparseMatrixCSR(ctrim)
  k.data = CuSparseMatrixCSR(dtrim)

end

function sparseindices(k :: CuSparseKernel)
  dcoo = CuSparseMatrixCOO(k.data)
  dcoo.rowInd, dcoo.colInd
end


## Extension of sparsify! for DenseKernels that carry CuArrays

# kernel data is made sparse purely on the GPU
function sparse_data_gpu!(kdata, trim)

  @debug "Sparsifying GPU kernel with trim = $trim" type=typeof(k)

  setzero!(kdata, trim)
  data = CuSparseMatrixCSR(kdata)
  CUDA.unsafe_free!(kdata) # release memory, but we must not use it again after this!

  data
end

# kernel data is made sparse on the CPU and then converted back - this may let
# us survive GPU memory pressure
function sparse_data_cpu!(kdata, trim)

  @debug "Sparsifying GPU kernel with trim = $trim on CPU" type=typeof(k)

  setzero!(kdata, trim)
  data = sparse(Array(kdata))
  CUDA.unsafe_free!(kdata)

  CuSparseMatrixCSR(data)
end

function sparsify!(k :: DenseKernel{T}, trim) where {T <: CuArray}

  # if there is enough free GPU space to hold an additional k.data array, we
  # assume that it is okay to create a sparse version of k.data on the GPU
  # before freeing the memory of k.data
  free, total = CUDA.Mem.info()
  if free >= sizeof(k.data)
    data = sparse_data_gpu!(k.data, trim)
  else
    data = sparse_data_cpu!(k.data, trim)
  end

  # get row and column indices of the data and construct sparse cost matrix
  dcoo = CuSparseMatrixCOO(data)
  lind = dcoo.rowInd .+ size(k.cost, 1) * (dcoo.colInd .- 1)
  cvals = k.cost[lind]
  # TODO: is it okay to share rowPtr and colVal here, or should we copy it?
  # Update: it seems to work well like this
  cost = CuSparseMatrixCSR(data.rowPtr, data.colVal, cvals, data.dims)

  CuSparseKernel(cost, data)
end

## Utility functions

# extend Base.setindex to CuArrays with logical selection 
# TODO: this (and related) functionality should be offered by the CUDA
# package itself. Consider a pull request?
function setscalar!(ys::CuArray{T}, x :: Real, bools::CuArray{Bool}) where {T}
  x = convert(T, x)
  bools = reshape(bools, prod(size(bools)))

  function kernel(ys::CuDeviceArray{T}, x, bools)
    i = threadIdx().x + (blockIdx().x - 1) * blockDim().x

    @inbounds if i <= length(bools) && bools[i]
      ys[i] = x
    end

    return
  end

  kernel = @cuda name="logical_setindex_scalar" launch=false kernel(ys, x, bools)
  config = launch_configuration(kernel.fun)
  threads = Base.min(length(bools), config.threads)
  blocks = cld(length(bools), threads)
  kernel(ys, x, bools; threads = threads, blocks = blocks)

  return ys
end

function setzero!(ys :: CuArray{T}, thr :: Real) where {T}
  thr = convert(T, thr)

  function kernel(ys::CuDeviceArray{T}, thr)
    i = threadIdx().x + (blockIdx().x - 1) * blockDim().x
    @inbounds if ys[i] <= thr ys[i] = 0 end
    return
  end

  kernel = @cuda name="setzero_trim" launch=false kernel(ys, thr)
  config = launch_configuration(kernel.fun)
  threads = Base.min(length(ys), config.threads)
  blocks = cld(length(ys), threads)
  kernel(ys, thr; threads = threads, blocks = blocks)

  return ys
end

# TODO: this function does not work as intended (values too low). Why?
function countzero(ys :: CuArray{T}, thr :: Real) where {T}
  thr = convert(T, thr)
  s = CuVector{Int32}([0])

  function kernel(ys :: CuDeviceArray{T}, thr, s :: CuDeviceArray{Int32})
    i = threadIdx().x + (blockIdx().x - 1) * blockDim().x
    @inbounds if ys[i] <= thr s[1] += 1 end
    return
  end

  kernel = @cuda name="countzero" launch=false kernel(ys, thr, s)
  config = launch_configuration(kernel.fun)
  threads = Base.min(length(ys), config.threads)
  blocks = cld(length(ys), threads)
  kernel(ys, thr, s; threads = threads, blocks = blocks)

  return CUDA.@allowscalar s[1]
end

