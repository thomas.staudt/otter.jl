

# ----- Kernels ------------------------------------------------------------- #

"""
Abstract type of a Sinkhorn kernel.
"""
abstract type AbstractKernel end

kimplement(k, m) = @error "Kernel $(typeof(k)) does not implement method `$m`"

"""
    costdata(k)

Cost data stored in the kernel `k`. The return type depends on the kernel
implementation and is expected to be `<: AbstractMatrix`. This function should
not be called directly.
"""
costdata(k :: AbstractKernel) = kimplement(k, :costdata)


"""
    kerneldata(k)

Kernel matrix stored in the kernel `k`. The return type depends on the kernel
implementation and is expected to be `<: AbstractMatrix`. This function should
not be called directly.
"""
kerneldata(k :: AbstractKernel) = kimplement(k, :costdata)

"""
    dense(k)

Check whether the kernel `k` uses a dense storage format or not.
"""
dense(k) = kimplement(k, :dense)

"""
    _plan(k)

Sinkhorn transport plan associated to the kernel `k`.
This function only works as intended if used on unmodified return values of the
algorithm and should not be called directly. Use `plan` on the result of one of
the `sinkhorn_*` routines instead.
"""
_plan(k :: AbstractKernel, d, reg) = kimplement(k, :_plan)

"""
    _cost(k)

Sinkhorn cost associated to the kernel `k`.
This function only works as intended if used on unmodified return values of the
algorithm and should not be called directly. Use `cost` on the result of one of
the `sinkhorn_*` routines instead.
"""
_cost(k :: AbstractKernel) = kimlement(k, :_cost)

"""
    trim!(k, threshold)

Trim a kernel `k` with threshold `threshold`. 
"""
trim!(k :: AbstractKernel, _) = kimplement(k, :trim!)

"""
    kernelinfo(k, threshold)

Provide information about the kernel `k`. The value `threshold` is used to calculate
the potential sparsity if `k` was trimmed (but the trimming does not take place).
This function will only work as intended on kernels that have absorbed the dual
solutions directly prior to the call to `kernelinfo`.
"""
kernelinfo(k :: AbstractKernel, args...) = kimplement(k, :kernelinfo)

"""
    sparsify!(k, threshold)

Convert the kernel `k` to a suitable sparse representation after trimming with
`threshold`. A new sparse kernel is returned. Note that this function may alter
the original kernel `k` for efficiency (and potentially make it unusable). It
has no effect on kernels that are already sparse.
"""
function sparsify!(k :: AbstractKernel, args...)
  @info "Cannot sparsify kernels of type $(typeof(k))."
  k
end


"""
    prepare!(k, reg)
    prepare!(k, da, db, reg)

Prepare the kernel `k` for application of the Sinkhorn algorithm with
regularization `reg`. This means that kernel values are calculated from the
stored cost values. If the duals `da` and `db` are provided, they will be
absorbed into the kernel. 
"""
prepare!(:: AbstractKernel, args...) = kimplement(k, :prepare!)

"""
    absorb!(k, da, db, reg)

Absorbs the duals `da` and `db` into the kernel `k`.
"""
absorb!(:: AbstractKernel, args...) = kimplement(k, :absorb!)


"""
    proxystep!(u, v, a, b, k)

One proximity step of the Sinkhorn iteration with scale variables `u` and `v`
for measures `a` and `b` and kernel `k`.
"""
function proxystep!( u :: AbstractVector
                   , v :: AbstractVector
                   , a :: AbstractVector
                   , b :: AbstractVector
                   , k :: AbstractKernel )

  data = kerneldata(k)

  mul!(u, data, v)
  u .= a ./ u
  mul!(v, data', u)
  v .= b ./ v

end

"""
    proxystep_symmetric!(u, v, u_buf, v_buf, a, b, k)

One symmetrized proximity step of the Sinkhorn iteration with scale variables
`u` and `v` for measures `a` and `b` and kernel `k`. The vectors `u_buf` and `v_buf`
are used as buffers.
"""
function proxystep_symmetric!( u :: AbstractVector
                             , v :: AbstractVector
                             , u_buf :: AbstractVector
                             , v_buf :: AbstractVector
                             , a :: AbstractVector
                             , b :: AbstractVector
                             , k :: AbstractKernel )

  data = kerneldata(k)

  mul!(u_buf, data, v)
  mul!(v_buf, data', u)
  u .= sqrt.(u) .* sqrt.(a ./ u_buf)
  v .= sqrt.(v) .* sqrt.(b ./ v_buf)

end

Base.size(k :: AbstractKernel) = size(kerneldata(k))
Base.length(k :: AbstractKernel) = length(kerneldata(k))

# ----- Dense Kernels ------------------------------------------------------- #

"""
Default implementation of dense kernels. It can handle cost and kernel data of
any concrete subtype of `AbstractMatrix` if necessary linear algebra operations
are implemented. Common types to use are `Matrix{Float64}`, or
`CuMatrix{Float32}` if the `CUDA` package is used.
"""
mutable struct DenseKernel{T <: AbstractMatrix} <: AbstractKernel
  cost :: T  # cost matrix
  data :: T  # kernel data matrix
end

function DenseKernel(c; atype = nothing, _dummy = false)

  @debug "Creating dense kernel from cost of type $(typeof(c))"
  _dummy && @debug "...it is a dummy kernel..."

  if isnothing(atype)
    atype = Array{eltype(c)}
  end

  cost = _dummy ? atype(undef, 0, 0) : convert(atype, costdata(c))

  DenseKernel{typeof(cost)}(cost, similar(cost))

end

function adapt_type(:: Type{T}, k :: DenseKernel) where {T <: AbstractArray}
  cost = convert(T, k.cost)
  data = convert(T, k.data)
  DenseKernel(cost, data)
end

costdata(k :: DenseKernel) = k.cost
kerneldata(k :: DenseKernel) = k.data
dense(k :: DenseKernel) = true

_plan(k :: DenseKernel) = k.data
_cost(k :: DenseKernel) = dot(k.data, k.cost)

trim!(k :: DenseKernel, _) = nothing

function kernelinfo(k :: DenseKernel, trim) 

  # Performance of this can probably be improved on GPU!
  sparsity = sum(x -> x <= trim, k.data) / length(k.data)

  ( max      = maximum(k.data)
  , min      = minimum(k.data)
  , sparsity = sparsity
  , memory   = sizeof(k.data) / 1024^2 
  , type     = typeof(k) )

end

function prepare!(k :: DenseKernel, reg)

  @debug "Prepare kernel with ε = $reg" type=typeof(k)
  k.data .= exp.(.- k.cost ./ reg)

end

function prepare!(k :: DenseKernel, da, db, reg)

  @debug "Absorb scale variables with ε = $reg" type=typeof(k)
  k.data .= exp.(.- (k.cost .- da .- db') ./ reg)

end

absorb!(k :: DenseKernel, da, db, reg) = prepare!(k, da, db, reg)


# ----- Sparse Kernels ------------------------------------------------------- #

"""
Abstract sparse kernels. Sparse kernels are beneficial in late iterations of the
Sinkhorn scale algorithm with low values of `reg`, as the kernel then is
practically sparse.

A small trimming threshold, like `1e-15`, has to be chosen to decide which
values of the kernel are considered zeros. Conversion to sparse kernels is
automatically conducted in the `sinkhorn_scale` and `sinkhorn_multires`
functions if a sparsity threshold is exceeded.

Additionally to the interface of `AbstractKernel`, an `AbstractSparseKernel`
must implement the function `sparseindices`.
"""
abstract type AbstractSparseKernel <: AbstractKernel end

"""
    sparseindices(k)

Return two vectors `I` and `J` such that the pairs `(i,j)` in `zip(I, J)`
enumerate the non-zero positions in the kernel matrix of `k`.
"""
sparseindices(k :: AbstractSparseKernel) = kimplement(k, :sparseindices)

dense(k :: AbstractSparseKernel) = false

# Function that removes close-to-zero entries in the kernel. This will only work
# as intended for kernels that have absorbed dual variables `du` and `dv` that
# are close to the correct solution of the Sinkhorn problem.
# In particular, trimming the support of the kernel is dangerous if reg is small
# and if we are not yet close to the true solution, as we then irreversibly
# restrict our transport plan to the wrong support.
function trim!(k :: AbstractSparseKernel, trim)

  @debug "Trimming with trim = $trim" type=typeof(k)

  # Only trim if the kernel's trim value is greater or equal to zero
  trim < 0 && return

  _trim!(k, trim)

end

function kernelinfo(k :: AbstractSparseKernel, trim)

  data = kerneldata(k)
  nz = nonzeros(data)

  ( max      = maximum(nz)
  , min      = nnz(data) < length(data) ? 0 : minimum(nz)
  , sparsity = (length(data) - nnz(data)) / length(data)
  , memory   = 3sizeof(nz) / 1024^2 # upper bound for size, also "counts" indices
  , type     = typeof(k) )

end


function prepare!(k :: AbstractSparseKernel, reg)

  @debug "Prepare kernel with ε = $reg" type=typeof(k)
  
  nk = nonzeros(kerneldata(k))
  nc = nonzeros(costdata(k))
  nk .= exp.( .- nc ./ reg )

  return
end

function prepare!(k :: AbstractSparseKernel, da, db, reg)

  @debug "Absorb scale variables with ε = $reg" type=typeof(k)

  I, J = sparseindices(k)
  nk = nonzeros(kerneldata(k))
  nc = nonzeros(costdata(k))
  nk .= exp.( .- (nc .- da[I] .- db[J]) ./ reg)
  
  return
end

absorb!(k :: AbstractSparseKernel, da, db, reg) = prepare!(k, da, db, reg)

"""
Sparse kernel implementation based on the `SparseMatrixCSC` type of the julia
standard library.
"""
struct SparseKernel{F} <: AbstractSparseKernel
  cost :: SparseMatrixCSC{F} # sparse base cost matrix
  data :: SparseMatrixCSC{F} # sparse data matrix / transport plan
end

costdata(k :: SparseKernel) = k.cost
kerneldata(k :: SparseKernel) = k.data

_plan(k :: SparseKernel) = k.data
_cost(k :: SparseKernel) = dot(nonzeros(k.cost), nonzeros(k.data))

function _trim!(k :: SparseKernel, trim)

  cn = nonzeros(k.cost)
  kn = nonzeros(k.data)

  idx = findall(x -> x <= trim, kn)

  cn[idx] .= -one(trim)
  kn[idx] .= -one(trim)

  SparseArrays.fkeep!(k.data, (_, _, v) -> v >= 0)
  SparseArrays.fkeep!(k.cost, (_, _, v) -> v >= 0)

end

sparseindices(k :: SparseKernel) = sparseindices(k.data)

# copied and modified from 
# https://github.com/JuliaLang/julia/blob/293e60cc985dd8a2752b7c5276ce4335050420e7/stdlib/SparseArrays/src/sparsematrix.jl 
function sparseindices(s :: SparseMatrixCSC{Tv,Ti}) where {Tv,Ti}
  numnz = nnz(s)
  I = Vector{Ti}(undef, numnz)
  J = Vector{Ti}(undef, numnz)
  cptr = SparseArrays.getcolptr(s)

  count = 1
  @inbounds for col = 1:size(s, 2), k = cptr[col]:(cptr[col+1]-1)
    I[count] = SparseArrays.rowvals(s)[k]
    J[count] = col
    count += 1
  end

  (I, J)
end


function sparsify!(k :: DenseKernel{<: Array}, trim)

  @debug "Sparsifying kernel with trim = $trim" type=typeof(k)
  
  # Map values smaller than trim to zero
  map!(x -> x <= trim ? 0 : x, k.data, k.data)

  # Create sparse data and cost matrices
  data = sparse(k.data)
  cost = similar(data)

  # Fill the sparse cost matrix with values of the original cost matrix
  I, J = sparseindices(cost)
  cdata = nonzeros(cost)
  cdata .= k.cost[I .+ size(cost, 1) * (J .- 1)]

  # Generate and return the sparse kernel
  SparseKernel(cost, data)

end

