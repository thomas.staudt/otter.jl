
Some sources for computational Sinkhorn distances / optimal transport
* https://mathematical-coffees.github.io/mc01-ot/
* https://regularize.wordpress.com/2015/09/17/calculating-transport-plans-with-sinkhorn-knopp/
* http://marcocuturi.net/SI.html
* http://papers.nips.cc/paper/6792-near-linear-time-approximation-algorithms-for-optimal-transport-via-sinkhorn-iteration.pdf [Greenkhorn]

Regularized OT for other regularizations than entropy
* https://arxiv.org/abs/1710.06276
* https://arxiv.org/abs/1903.01112

"Better" Algorithms to calculate regularized Wasserstein?
* https://arxiv.org/abs/1901.06482
* https://arxiv.org/abs/1802.04367

Other packages with similar goals:
* https://github.com/gpeyre/SinkhornAutoDiff
* https://pot.readthedocs.io/en/stable/

"Sharp" Sinkhorn distances and its gradients
* https://papers.nips.cc/paper/7827-differential-properties-of-sinkhorn-approximation-for-learning-with-wasserstein-distance.pdf

Deep learning
* https://arxiv.org/pdf/1706.00292.pdf [Sinkhorn generative models]
* https://arxiv.org/abs/1810.01118 [Sinkhorn Autoencoders]
* http://papers.nips.cc/paper/5679-learning-with-a-wasserstein-loss [on graphs]
